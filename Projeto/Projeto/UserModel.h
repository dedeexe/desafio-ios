//
//  UserModel.h
//  Projeto
//
//  Created by dede.exe on 10/23/15.
//  Copyright © 2015 Pancepsys. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Mantle.h"

@interface UserModel : MTLModel <MTLJSONSerializing>

@property(nonatomic, assign) NSUInteger   id;
@property(nonatomic, strong) NSString     *name;
@property(nonatomic, strong) NSURL        *avatarURL;
@property(nonatomic, strong) NSString     *bio;

@property(nonatomic, strong) NSData *imageData;


-(BOOL)loadImageData;

@end
