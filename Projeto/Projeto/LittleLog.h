//
//  LittleLog.h
//  Projeto
//
//  Created by dede.exe on 10/22/15.
//  Copyright © 2015 Pancepsys. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LittleLog : NSObject {
    NSString *entityName;
}

-(id)initWithIdentifier:(NSString *)identifier;
-(id)initWithObject:(NSObject *)object;

-(void)log:(NSString *)logString;

@end
