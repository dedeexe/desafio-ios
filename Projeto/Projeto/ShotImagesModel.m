//
//  ShotImagesModel.m
//  Projeto
//
//  Created by dede.exe on 10/23/15.
//  Copyright © 2015 Pancepsys. All rights reserved.
//

#import "ShotImagesModel.h"
#import "LittleLog.h"

@interface ShotImagesModel ()
{
    LittleLog *log;
}
@end

@implementation ShotImagesModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"normal": @"normal",
             @"teaser": @"teaser"
            };
}



-(instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError *__autoreleasing *)error
{
    
    self  = [super initWithDictionary:dictionaryValue error:error];
    
    if (self)
    {
        log = [[LittleLog alloc] initWithObject: self];
    }
    
    return self;
    
}


-(BOOL)loadImageData
{
    NSData *data = [NSData dataWithContentsOfURL:self.normal];
    if (data != nil)
    {
        self.imageData = data;
        
        NSString *logStr = [NSString stringWithFormat:@"Image Loaded: %@", self.normal ];
        [log log:logStr];
        return TRUE;
    }
    
    NSString *logStr = [NSString stringWithFormat:@"Fail to loaded image: %@", self.normal ];
    [log log:logStr];
    
    return FALSE;
}


@end
