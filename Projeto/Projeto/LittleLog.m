//
//  LittleLog.m
//  Projeto
//
//  Created by dede.exe on 10/22/15.
//  Copyright © 2015 Pancepsys. All rights reserved.
//

#import "LittleLog.h"

@implementation LittleLog

-(id)initWithObject:(NSObject *)object
{
    return [self initWithIdentifier:NSStringFromClass(object.class)];
}


-(id)initWithIdentifier:(NSString *)identifier
{
    self = [super init];
    
    if (self)
    {
        entityName = [NSString stringWithString:identifier];
    }
    
    return self;
}

-(void)log:(NSString *)logString
{
    NSString *str = [NSString stringWithFormat:@"[%@] %@", entityName, logString];
    NSLog(@"%@", str);
}

@end
