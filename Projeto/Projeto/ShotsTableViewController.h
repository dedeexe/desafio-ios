//
//  ShotsTableViewController.h
//  Projeto
//
//  Created by dede.exe on 10/22/15.
//  Copyright © 2015 Pancepsys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShotsTableViewController : UITableViewController <UISplitViewControllerDelegate>

@end
