//
//  DetailedTableViewController.m
//  Projeto
//
//  Created by dede.exe on 10/24/15.
//  Copyright © 2015 Pancepsys. All rights reserved.
//

#import "DetailedTableViewController.h"

#import "DribbleTableCell.h"
#import "AuthorTableCell.h"
#import "DescriptionTableViewCell.h"
#import "NSString+HTMLParsing.h"
#import "LittleLog.h"


#define SECTION_SHOT        0
#define SECTION_AUTHOR      1
#define SECTION_DESC        2

@interface DetailedTableViewController ()
{
    LittleLog *log;
}

@end

@implementation DetailedTableViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    log = [[LittleLog alloc] initWithObject: self];
    [log log:@"Detail View Loaded"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (_shot == nil)
    {
        
        [log log:@"No data selected"];

        
        UILabel *lblNoData = [[UILabel alloc] initWithFrame:CGRectMake(0,
                                                                      0,
                                                                      self.tableView.bounds.size.width,
                                                                      self.tableView.bounds.size.height)];
        
        lblNoData.text = @"Select a Shot in the left list";
        lblNoData.textAlignment = NSTextAlignmentCenter;
        lblNoData.textColor = [UIColor grayColor];
        
        self.tableView.backgroundView = lblNoData;
        return 0;
    }
    
    [log log:@"Data selected"];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case SECTION_SHOT:
            {       //Image
                
                [log log:@"Loading Shot Table Cell."];
            
                DribbleTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DribbbleDetailTableCell"];
                
                if(!cell)
                {
                    [self registerTableCells];
                    cell = [tableView dequeueReusableCellWithIdentifier:@"DribbbleDetailTableCell"];
                }
                
                cell.title.text = _shot.title;
                cell.views.text = [NSString stringWithFormat:@"%lu", (unsigned long)_shot.views];
                [self loadImageAsync:_shot toImageView:cell.viewImage];
                
                return cell;

            }
        
        case SECTION_AUTHOR:
            {       //Author informations
                [log log:@"Loading Author Table Cell."];
                
                AuthorTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AuthorTableCell"];
                
                if(!cell)
                {
                    [self registerTableCells];
                    cell = [tableView dequeueReusableCellWithIdentifier:@"AuthorTableCell"];
                }
                
                cell.authorName.text = _shot.userInfo.name;
                [self loadUserImageAsync:_shot toImageView:cell.authorImage];
        
                cell.authorImage.layer.masksToBounds = TRUE;
                cell.authorImage.layer.cornerRadius = cell.authorImage.frame.size.width / 2;
        
                return cell;
            }

        case SECTION_DESC:
        {       //Shot Description informations
            
            [log log:@"Loading Description Table Cell."];

            
            DescriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DescriptionTableViewCell"];
            
            if(!cell)
            {
                [self registerTableCells];
                cell = [tableView dequeueReusableCellWithIdentifier:@"DescriptionTableViewCell"];
            }
            
            cell.desc.text = [NSString stringByRemovingAllHtmlTags: _shot.desc];
            
            return cell;
        }
            
        default:
            break;
    }

    return nil;
    
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    CGFloat ret = 0;
    
    switch(indexPath.row)
    {
        case SECTION_SHOT:
            ret = 250;
            break;
            
        case SECTION_AUTHOR:
            ret = 80;
            break;
            
        case SECTION_DESC:
            ret = 250;
            break;
    }
    
    return ret;
    
}


-(void) registerTableCells {
    
    [self.tableView registerNib:[UINib nibWithNibName:@"DribbbleDetailTableCell" bundle:nil] forCellReuseIdentifier:@"DribbbleDetailTableCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"AuthorDetailTableCell" bundle:nil] forCellReuseIdentifier:@"AuthorTableCell"];

    [self.tableView registerNib:[UINib nibWithNibName:@"DescriptionTableCell" bundle:nil] forCellReuseIdentifier:@"DescriptionTableViewCell"];
    
}



#pragma mark - Async Load Images
-(void)loadImageAsync:(ShotModel *)shotItem toImageView:(UIImageView *)imgView
{
 
    [log log:@"Loading Asyncrhonous Shot image."];
    
    dispatch_queue_t imageQueue = dispatch_queue_create("com.projeco.downloadImage", nil);
    
    dispatch_async(imageQueue, ^{
        
        //Don't let image be loaded twice ( a kind of a poor cache )
        if (shotItem.images.imageData == nil)
        {
            [shotItem.images loadImageData];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            imgView.image = [UIImage imageWithData:shotItem.images.imageData];
            [log log:@"Shot image loaded."];

        });
        
    });
}


-(void)loadUserImageAsync:(ShotModel *)shotItem toImageView:(UIImageView *)imgView
{
    
    [log log:@"Loading Asyncrhonous Author image."];
    
    dispatch_queue_t imageQueue = dispatch_queue_create("com.projeco.downloadImage", nil);
    
    dispatch_async(imageQueue, ^{
        
        //Don't let image be loaded twice ( a kind of a poor cache )
        if (shotItem.userInfo.imageData == nil)
        {
            [shotItem.userInfo loadImageData];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            imgView.image = [UIImage imageWithData:shotItem.userInfo.imageData];
            [log log:@"Author image loaded."];
        });
        
    });
}


@end
