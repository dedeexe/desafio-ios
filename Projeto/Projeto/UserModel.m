//
//  UserModel.m
//  Projeto
//
//  Created by dede.exe on 10/23/15.
//  Copyright © 2015 Pancepsys. All rights reserved.
//

#import "UserModel.h"
#import "LittleLog.h"

@interface UserModel ()
{
    LittleLog *log;
}
@end

@implementation UserModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"id": @"id",
             @"name": @"name",
             @"avatarURL": @"avatar_url",
             @"bio": @"bio",
             };
}


-(instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError *__autoreleasing *)error
{
    
    self  = [super initWithDictionary:dictionaryValue error:error];
    
    if (self)
    {
        log = [[LittleLog alloc] initWithObject: self];
    }
    
    return self;
    
}


-(BOOL)loadImageData
{
    NSData *data = [NSData dataWithContentsOfURL:self.avatarURL];
    if (data != nil)
    {
        self.imageData = data;
        
        NSString *logStr = [NSString stringWithFormat:@"Image Loaded: %@", self.avatarURL ];
        [log log:logStr];
        return TRUE;
    }
    
    NSString *logStr = [NSString stringWithFormat:@"Fail to loaded image: %@", self.avatarURL ];
    [log log:logStr];
    
    return FALSE;
}


@end
