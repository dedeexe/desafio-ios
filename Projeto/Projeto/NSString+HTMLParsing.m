//
//  NSString+HTMLParsing.m
//  Projeto
//
//  Created by dede.exe on 10/24/15.
//  Copyright © 2015 Pancepsys. All rights reserved.
//

#import "NSString+HTMLParsing.h"


@implementation NSString (HTMLParsing)


+(NSString *)stringByRemovingAllHtmlTags:(NSString *)htmlString
{

    NSScanner *scanner;
    NSString *text = nil;
    scanner = [NSScanner scannerWithString:htmlString];
    
    while ([scanner isAtEnd] == NO) {
        [scanner scanUpToString:@"<" intoString:NULL] ;
        [scanner scanUpToString:@">" intoString:&text] ;
        htmlString = [htmlString stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    
    htmlString = [htmlString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return htmlString;
    
}

@end
