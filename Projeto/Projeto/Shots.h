//
//  ContentManager.h
//  Projeto
//
//  Created by dede.exe on 10/21/15.
//  Copyright © 2015 Pancepsys. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AFNetworking.h"

@interface Shots : NSObject {
    AFHTTPSessionManager *sessionManager;
    AFHTTPRequestOperationManager *op;
}

-(instancetype)initWithToken:(NSString *)token;
-(void) load:(NSUInteger)page completion:(void (^)(id responseObj))completion;

@end
