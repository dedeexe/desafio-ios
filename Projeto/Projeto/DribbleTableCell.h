//
//  DribbleTableCell.h
//  Projeto
//
//  Created by dede.exe on 10/23/15.
//  Copyright © 2015 Pancepsys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DribbleTableCell : UITableViewCell

@property(nonatomic, weak) IBOutlet UILabel *title;
@property(nonatomic, weak) IBOutlet UILabel *views;

@property(nonatomic, weak) IBOutlet UIImageView *viewImage;

@end
