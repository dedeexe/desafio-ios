//
//  ShotsTableViewController.m
//  Projeto
//
//  Created by dede.exe on 10/22/15.
//  Copyright © 2015 Pancepsys. All rights reserved.
//

#import "defines.h"
#import "ShotsTableViewController.h"
#import "Shots.h"
#import "ShotModel.h"
#import "UserModel.h"
#import "LittleLog.h"
#import "DribbleTableCell.h"
#import "DetailedTableViewController.h"


@interface ShotsTableViewController ()
{
    Shots *shots;
    NSMutableArray *shotList;
    LittleLog *log;
    
    UIView *refreshLoadingView;
    NSUInteger page;
    
    BOOL canBottomRefresh;
    
}

@end

@implementation ShotsTableViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    log = [[LittleLog alloc] initWithObject: self];
    [log log:@"Shot List View controller Loaded"];
    
    page = 1;
    canBottomRefresh = FALSE;
    
    //Configure UISplitviewController
    //I want the master view openning in app launch
    self.splitViewController.delegate = self;
    self.splitViewController.preferredDisplayMode = UISplitViewControllerDisplayModeAllVisible;
    
    //Configure Table View
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 250.0;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];

    
    //Configure Pull To Refresh
    [self configureRefreshControl];
    
    //Begin to load shots
    shotList = [NSMutableArray new];
    shots = [[Shots alloc] initWithToken:DRIBBLE_KEY];
    [self loadShots:page];
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ScrollView Delegates
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //Check if reach the end of scroll
    if(scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.bounds.size.height)) {
        if (canBottomRefresh)
        {
            canBottomRefresh = FALSE;
            [log log:@"Downloading new shots"];
            page++;
            [self loadShots:page];
        }
    }
}

#pragma mark - Table view data source and Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [shotList count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DribbleTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DribbbleCell"];
    
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"DribbbleTableCell" bundle:nil] forCellReuseIdentifier:@"DribbbleCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"DribbbleCell"];
    }
    
    ShotModel *item = (ShotModel *)[shotList objectAtIndex:indexPath.row];
    
    cell.title.text = item.title;
    cell.views.text = [NSString stringWithFormat:@"%lu", (unsigned long)item.views];
    [self loadImageAsync:item toImageView:cell.viewImage];
    
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"segueDetail" sender:nil];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *indexPathLast = (NSIndexPath *)[[tableView indexPathsForVisibleRows] lastObject];
    
    if (indexPath.row == indexPathLast.row)
    {
        canBottomRefresh = TRUE;
        [log log:@"End of list reached"];
    }
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UINavigationController *destinationView = (UINavigationController *) segue.destinationViewController;
    DetailedTableViewController *detailVC = (DetailedTableViewController *)destinationView.viewControllers[0];
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    detailVC.shot = (ShotModel *)[shotList objectAtIndex:indexPath.row];
}


#pragma mark - UISplitViewController delegate
-(BOOL)splitViewController:(UISplitViewController *)splitViewController collapseSecondaryViewController:(UIViewController *)secondaryViewController ontoPrimaryViewController:(UIViewController *)primaryViewController
{
    return TRUE;
}


#pragma mark - Internal Methods

-(void)loadShots:(NSUInteger)pageNumber
{
    [log log:@"Loading Shots"];
    
    [shots load:pageNumber
     completion:^(NSArray *responseObj) {
        
        NSString *str = [NSString stringWithFormat:@"%lu shot(s) loaded.", (unsigned long)responseObj.count];
        [log log:str];
        
        for (NSDictionary *item in responseObj)
        {
            NSError *error;
            ShotModel *shot = [MTLJSONAdapter modelOfClass:ShotModel.class
                                        fromJSONDictionary:item
                                                     error:&error];
            
            if (error)
            {

            }
            
            [shotList addObject:shot];
        }
        
        [self.tableView reloadData];
        [self.refreshControl endRefreshing];
    
         [log log:@"Shots Loaded"];
         
    }];
    
}

-(void)loadImageAsync:(ShotModel *)shotItem toImageView:(UIImageView *)imgView
{

    dispatch_queue_t imageQueue = dispatch_queue_create("com.projeco.downloadImage", nil);
    
    dispatch_async(imageQueue, ^{
        
        //Don't let image be loaded twice ( a kind of a poor cache )
        if (shotItem.images.imageData == nil)
        {
            [shotItem.images loadImageData];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            imgView.image = [UIImage imageWithData:shotItem.images.imageData];
        });
        
    });
    
}


#pragma mark - Configuration Functions

-(void) configureRefreshControl {
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    
    refreshLoadingView = [[UIView alloc] initWithFrame:self.refreshControl.bounds];
    refreshLoadingView.backgroundColor = [UIColor grayColor];
    [self.refreshControl addSubview:refreshLoadingView];
    
    [self.refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    
}

-(void)refresh:(id)sender {
    
    double delayInSeconds = 3.0;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

        [shotList removeAllObjects];
        
        [self loadShots:1];
        
    });
    
}

@end
