//
//  DetailedTableViewController.h
//  Projeto
//
//  Created by dede.exe on 10/24/15.
//  Copyright © 2015 Pancepsys. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShotModel.h"

@interface DetailedTableViewController : UITableViewController

@property(nonatomic, strong) ShotModel *shot;

@end
