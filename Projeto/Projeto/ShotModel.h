//
//  ShotModel.h
//  Projeto
//
//  Created by dede.exe on 10/23/15.
//  Copyright © 2015 Pancepsys. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Mantle.h"
#import "ShotImagesModel.h"
#import "UserModel.h"

@interface ShotModel : MTLModel <MTLJSONSerializing>

@property(nonatomic, assign) NSUInteger         id;
@property(nonatomic, strong) NSString           *title;
@property(nonatomic, strong) NSString           *desc;
@property(nonatomic, strong) ShotImagesModel    *images;
@property(nonatomic, assign) NSUInteger         views;

@property(nonatomic, strong) UserModel          *userInfo;

@end
