//
//  AuthorTableCell.h
//  Projeto
//
//  Created by dede.exe on 10/24/15.
//  Copyright © 2015 Pancepsys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AuthorTableCell : UITableViewCell

@property(nonatomic, weak) IBOutlet UILabel *authorName;
@property(nonatomic, weak) IBOutlet UIImageView *authorImage;

@end
