//
//  ContentManager.m
//  Projeto
//
//  Created by dede.exe on 10/21/15.
//  Copyright © 2015 Pancepsys. All rights reserved.
//

#import "Shots.h"
#import "LittleLog.h"

@interface Shots () {
    NSString *tokenString;
    LittleLog *log;
}


@end

@implementation Shots

- (instancetype)initWithToken:(NSString *)token;
{
    self = [super init];
    if (self) {
        
        tokenString = token;
        
        //TODO: Change it to a parameter
        sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://api.dribbble.com/v1/"]];
        sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        log = [[LittleLog alloc] initWithObject:self];        
    }
    return self;
}


-(void)load:(NSUInteger)page completion:(void (^)(id responseObj))completion
{
    NSString *requestString = [NSString stringWithFormat:@"shots?page=%lu&per_page=50&access_token=%@", (unsigned long)page, tokenString];
    
    [sessionManager GET: requestString
             parameters:nil
                success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
                    
                    [log log:@"Shots Request was success"];
                    
                    completion(responseObject);
                    
                }
                failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {

                    [log log:@"Shots Request failed!"];
                    
                }];
}
    

@end
