//
//  DescriptionTableViewCell.h
//  Projeto
//
//  Created by dede.exe on 10/25/15.
//  Copyright © 2015 Pancepsys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DescriptionTableViewCell : UITableViewCell

@property(nonatomic, weak) IBOutlet UILabel *desc;

@end
