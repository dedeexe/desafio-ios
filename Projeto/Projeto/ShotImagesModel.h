//
//  ShotImagesModel.h
//  Projeto
//
//  Created by dede.exe on 10/23/15.
//  Copyright © 2015 Pancepsys. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Mantle.h"

@interface ShotImagesModel : MTLModel <MTLJSONSerializing>

@property(nonatomic, strong) NSURL  *normal;
@property(nonatomic, strong) NSURL  *teaser;

@property(nonatomic, strong) NSData *imageData;

-(BOOL)loadImageData;

@end
