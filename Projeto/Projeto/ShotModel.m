//
//  ShotModel.m
//  Projeto
//
//  Created by dede.exe on 10/23/15.
//  Copyright © 2015 Pancepsys. All rights reserved.
//

#import "ShotModel.h"
#import "LittleLog.h"

@interface ShotModel ()
{
    LittleLog *log;
}
@end


@implementation ShotModel


+(NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"id":         @"id",
             @"title":      @"title",
             @"desc":       @"description",
             @"images":     @"images",
             @"views":      @"views_count",
             @"userInfo":   @"user"
             };
}


-(instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError *__autoreleasing *)error
{
    self  = [super initWithDictionary:dictionaryValue error:error];
    
    if (self)
    {
        log = [[LittleLog alloc] initWithObject:self];
    }
    
    return self;
}


@end
