//
//  LittleLogTest.m
//  Projeto
//
//  Created by dede.exe on 10/26/15.
//  Copyright © 2015 Pancepsys. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "LittleLog.h"

@interface LittleLogTest : XCTestCase
{
    LittleLog *log;
}

@end

@implementation LittleLogTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    log = [[LittleLog alloc] initWithIdentifier:@"TestLog"];
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testLittleLog {

    XCTAssertNotNil(log);

}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
