//
//  NSStringHTMLTest.m
//  Projeto
//
//  Created by dede.exe on 10/26/15.
//  Copyright © 2015 Pancepsys. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSString+HTMLParsing.h"

@interface NSStringHTMLTest : XCTestCase

@end

@implementation NSStringHTMLTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testNSStringHTML {

    NSString *html = @"<html><head><title>A</title></head><body bgcolor=\"#000000\"><h1>B</h1><p>C</P></p></body></html>";
    
    NSString *plainText = [NSString stringByRemovingAllHtmlTags:html];
    XCTAssertTrue([plainText isEqualToString:@"ABC"]);
    
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
